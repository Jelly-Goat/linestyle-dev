# linestyle-dev

Linestyle Script Language development repository.  
这里是lss的开发仓库。

欢迎到这里查阅我们的wiki，并提出你们的建议，或是提交pull request.  

## Feature | 特性

直观简洁的语言，不需要太多的英文基础。  

基于C++编写，翻译成C++并编译运行，或是基于C++编写的解释器运行。

是个垃圾。

## Quick-start | 快速搭建你的环境

### 1.1 搭建翻译并编译运行的环境

搭建好g++或者其他C++编译器

将g++添加到你的path中，或是在`{lssRootDir}/cfg/trans.json`中指定C++编译器。

### 1.2 搭建解释器环境

将release出的压缩包放置于一个路径较短的位置，并将`{lssRootDir}/bin/`添加到path，  
解释器即`{lssRootDir}/bin/linein` or `{lssRootDir/bin/linein.exe`.

### 2. 写一个垃圾程序

```lss
<--- std;

main <=[args: String[]] --> Int 
{
    std.println("Hello world!");
}
```

## 开源许可相关

~~What you f**k wanted to do, then do it(大雾).~~  
禁止任何对linestyle的商业用途，除非是以此为基础的扩展等或是获得  
包括Jelly_Goat在内的任意指定人员指定的许可项目。